public class readystock extends kue {
    private double jumlah;

    // konstruktor kelas readystock yang mengambil tiga parameter: name (nama kue), price (harga per kue), dan jumlah (jumlah kue)
    public readystock(String name, double price, double jumlah) {
        super(name, price);
        this.jumlah = jumlah;
    }

    public double getJumlah() {
        return this.jumlah;
    }

    public void setJumlah(double jumlah) {
        this.jumlah = jumlah;
    }

    //method yang menghitung total harga dari kue dalam stok yang tersedia
    public double hitungHarga() {
        return this.getPrice() * this.jumlah * 2.0;
    }

    public double Berat() {
        return 0.0;
    }  // mengembalikan nilai 0.0

    public double Jumlah() {
        return this.jumlah;
    } //mengembalikan nilai dari variabel instance jumlah.

    //method untuk mengambil representasi string dari superclass menggunakan kata kunci super.toString() dan menambahkan informasi tentang jumlah kue dan total harga menggunakan format string.
    public String toString() {
        String var10000 = super.toString();
        return var10000 + String.format("\nJumlah\t\t:%.2f\nTotal Harga\t:%.2f", this.jumlah, this.hitungHarga());
    }

}
