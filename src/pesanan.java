public class pesanan extends kue {
    private  double berat;


    public pesanan(String name, double price, double berat) {
        super(name, price);
        this.berat = berat;
    }
    public double getBerat() {
        return berat;
    }

    public void setBerat(double berat) {
        this.berat = berat;
    }
    @Override
    // method untuk menghitung harga dari kue pesanan
    public double hitungHarga() {
        return getPrice() * berat;
    }

    @Override
    // untuk mengembalikan nilai berat
    public double Berat() {
        return berat;
    }

    @Override
    // karena jumlah tidak di pakai pada kelas pesana, maka nilainya dikembalikan sebagai 0
    public double Jumlah() {
        return 0;
    }

    @Override
    // method untuk menampilkan output informasi kue yg dipesan yang terdiri dari nama kue, harga, dan berat
    public String toString() {
        return super.toString() + String.format("\nBerat\t\t:%.2f\nTotal Harga\t:%.2f", berat, hitungHarga());

    }
}
