public abstract class kue {
    private String name;
    private double price;

    public kue(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public abstract double hitungHarga();

    public abstract double Berat();

    public abstract double Jumlah();

    public String toString() {
        return String.format("\n---------- " + this.getName() + " ----------\nHarga\t\t:" + this.getPrice());
    }
}
